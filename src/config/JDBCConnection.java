/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package config;

import constand.MySQLConstand;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author nguyen
 */
public class JDBCConnection {

  public static Connection getJDBCConnection() {
    try {
      Class.forName(MySQLConstand.CLASS_NAME);
      return DriverManager
          .getConnection(MySQLConstand.URL, MySQLConstand.USER_NAME, MySQLConstand.PASSWORD);
    } catch (ClassNotFoundException | SQLException ex) {
      ex.printStackTrace();
    }
    return null;
  }

  public static void main(String[] args) {
    Connection connection = getJDBCConnection();

    if (connection != null) {
      System.out.println("Data connection successful");
    } else {
      System.out.println("Data connection failed");
    }
  }
}
