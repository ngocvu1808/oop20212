package dto.user;

/**
 * @author nguyen
 * @create_date 29/04/2022
 */
public class LoginRequest {

  private String userName;
  private String password;

  public LoginRequest(String userName, String password) {
    this.userName = userName;
    this.password = password;
  }

  public LoginRequest() {
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }


}
