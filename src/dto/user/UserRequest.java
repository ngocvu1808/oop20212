package dto.user;

/**
 * @author nguyen
 * @create_date 29/04/2022
 */
public class UserRequest {
  private String fullName;
  private String userName;
  private String password;
  private String retypePass;
  private String phone;
  private String gender;
  private String address;
  private String role;
  private int age;

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public UserRequest() {
  }

  public UserRequest(String fullName, String userName, String password, String retypePass,
      String phone, String gender, String address, String role) {
    this.fullName = fullName;
    this.userName = userName;
    this.password = password;
    this.retypePass = retypePass;
    this.phone = phone;
    this.gender = gender;
    this.address = address;
    this.role = role;
  }

  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getRetypePass() {
    return retypePass;
  }

  public void setRetypePass(String retypePass) {
    this.retypePass = retypePass;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }
}
