package dto.user;

/**
 * @author nguyen
 * @create_date 29/04/2022
 */
public class UserResponse {
  private int id;
  private String userName;
  private int age;
  private String fullName;
  private String phone;
  private String role;
  private String gender;

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public UserResponse(int id, String userName, int age, String fullName) {
    this.id = id;
    this.userName = userName;
    this.age = age;
    this.fullName = fullName;
  }

  public UserResponse() {
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }
}
