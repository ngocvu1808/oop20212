package dto.staff;

import constand.enumm.Gender;
import constand.enumm.StaffStatus;

/**
 * @author nguyen
 * @create_date 27/05/2022
 */
public class StaffRequest {

  private String name;
  private String username;
  private String password;
  private String code;
  private String phone;
  private String address;
  private String salary;
  private StaffStatus status;
  private Gender gender;
  private int age;

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public Gender getGender() {
    return gender;
  }

  public void setGender(Gender gender) {
    this.gender = gender;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getSalary() {
    return salary;
  }

  public void setSalary(String salary) {
    this.salary = salary;
  }

  public StaffStatus getStatus() {
    return status;
  }

  public void setStatus(StaffStatus status) {
    this.status = status;
  }
}
