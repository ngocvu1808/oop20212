package repo.impl;

import config.JDBCConnection;
import dto.user.LoginRequest;
import dto.user.LogoutRequest;
import dto.user.UserRequest;
import dto.user.UserResponse;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import repo.UserRepo;

/**
 * @author nguyen
 * @create_date 29/04/2022
 */
public class UserRepoImpl implements UserRepo {

  @Override
  public void login(LoginRequest dto) {

  }

  @Override
  public void logout(LogoutRequest dto) {

  }

  @Override
  public UserResponse getById(int id){
    Connection connection = JDBCConnection.getJDBCConnection();
    PreparedStatement pst = null;

    String sql = "SELECT * FROM dto WHERE ID = ?";

    try {
      pst = connection.prepareStatement(sql);
      pst.setInt(1, id);
      ResultSet rs = pst.executeQuery();

      while (rs.next()) {
        UserResponse response = new UserResponse();

        response.setId(rs.getInt("ID"));
        response.setFullName(rs.getString("FULLNAME"));
        response.setAge(rs.getInt("AGE"));
        response.setUserName(rs.getString("USERNAME"));
        response.setPhone(rs.getString("PHONENUMBER"));
        response.setGender(rs.getString("GENDER"));
        response.setRole(rs.getString("ROLE"));

        return response;
      }

    } catch (Exception ex) {
      ex.printStackTrace();
    } finally {
      if (pst != null) {
        try {
          pst.close();
        } catch (SQLException ex) {
          ex.printStackTrace();
        }
      }
      if (connection != null) {
        try {
          connection.close();
        } catch (SQLException ex) {
          ex.printStackTrace();
        }
      }
    }
    return null;
  }

  @Override
  public int addUser(UserRequest dto) {
    Connection connection = JDBCConnection.getJDBCConnection();
    PreparedStatement pst = null;
    String sql = "INSERT INTO user (FULLNAME, AGE, USERNAME, PASSWORD, RETYPEPASS, PHONENUMBER, GENDER, ADDRESS, ROLE) VALUE(?,?,?,?,?,?,?,?,?)";

    try {
      pst = connection.prepareStatement(sql);
      pst.setString(1, dto.getFullName());
      pst.setInt(2, dto.getAge());
      pst.setString(3, dto.getUserName());
      pst.setString(4, dto.getPassword());
      pst.setString(5, dto.getRetypePass());
      pst.setString(6, dto.getPhone());
      pst.setString(7, dto.getGender());
      pst.setString(8, dto.getAddress());
      pst.setString(9, dto.getRole());

      int rs = pst.executeUpdate();
      System.out.println(rs);

    } catch (Exception ex) {
      ex.printStackTrace();
    } finally {
      if (pst != null) {
        try {
          pst.close();
        } catch (SQLException ex) {
          ex.printStackTrace();
        }
      }
      if (connection != null) {
        try {
          connection.close();
        } catch (SQLException ex) {
          ex.printStackTrace();
        }
      }
    }
    return 0;
  }

  @Override
  public int updateUser(int userId, UserRequest dto) {
    Connection connection = JDBCConnection.getJDBCConnection();
    PreparedStatement pst = null;
    String sql = "UPDATE user SET FULLNAME = ?,AGE = ?,USERNAME = ?,PASSWORD = ?,RETYPEPASS = ?,PHONENUMBER = ?,GENDER = ?,ADDRESS = ?, ROLE = ? WHERE ID =?";

    try {
      pst = connection.prepareStatement(sql);
      pst = connection.prepareStatement(sql);
      pst.setString(1, dto.getFullName());
      pst.setInt(2, dto.getAge());
      pst.setString(3, dto.getUserName());
      pst.setString(4, dto.getPassword());
      pst.setString(5, dto.getRetypePass());
      pst.setString(6, dto.getPhone());
      pst.setString(7, dto.getGender());
      pst.setString(8, dto.getAddress());
      pst.setString(9, dto.getRole());
      pst.setInt(10, userId);

      int rs = pst.executeUpdate();
      System.out.println(rs);
    } catch (Exception ex) {
      ex.printStackTrace();
    } finally {
      if (pst != null) {
        try {
          pst.close();
        } catch (SQLException ex) {
          ex.printStackTrace();
        }
      }
      if (connection != null) {
        try {
          connection.close();
        } catch (SQLException ex) {
          ex.printStackTrace();
        }
      }
    }
    return 0;
  }

  @Override
  public void deleteUser(int id) {
    Connection connection = JDBCConnection.getJDBCConnection();
    PreparedStatement pst = null;
    String sql = "update User set is_deleted = '1' where id = ?";
    try {
      pst = connection.prepareStatement(sql);
      pst.setInt(1, id);
      int rs = pst.executeUpdate();
      System.out.println(rs);
    } catch (Exception ex) {
      ex.printStackTrace();
    } finally {
      if (pst != null) {
        try {
          pst.close();
        } catch (SQLException ex) {
          ex.printStackTrace();
        }
      }
      if (connection != null) {
        try {
          connection.close();
        } catch (SQLException ex) {
          ex.printStackTrace();
        }
      }
    }
  }

  @Override
  public List<UserResponse> search(String value) {
    List<UserResponse> results = new ArrayList<>();

    Connection connection = JDBCConnection.getJDBCConnection();
    PreparedStatement pst = null;

    String sql = "SELECT * FROM `user` WHERE CONCAT (`ID`, `FULLNAME`, `AGE`, `USERNAME`, `PHONENUMBER`) LIKE '%"+value+"%' ";

    try {
      pst = connection.prepareStatement(sql);
      ResultSet rs = pst.executeQuery();

      while (rs.next()) {
        UserResponse user = new UserResponse();

        user.setId(rs.getInt("ID"));
        user.setFullName(rs.getString("FULLNAME"));
        user.setAge(rs.getInt("AGE"));
        user.setUserName(rs.getString("USERNAME"));
        user.setPhone(rs.getString("PHONENUMBER"));
        user.setGender(rs.getString("GENDER"));
        user.setRole(rs.getString("ROLE"));

        results.add(user);
      }

    } catch (Exception ex) {
      ex.printStackTrace();
    } finally {
      if (pst != null) {
        try {
          pst.close();
        } catch (SQLException ex) {
          ex.printStackTrace();
        }
      }
      if (connection != null) {
        try {
          connection.close();
        } catch (SQLException ex) {
          ex.printStackTrace();
        }
      }
    }
    return results;
  }
}
