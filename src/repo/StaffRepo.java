package repo;

import dto.staff.StaffRequest;

/**
 * @author nguyen
 * @create_date 27/05/2022
 */
public interface StaffRepo {

  int addStaff(StaffRequest staff);

}
