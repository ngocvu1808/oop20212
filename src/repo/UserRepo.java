package repo;

import dto.user.LoginRequest;
import dto.user.LogoutRequest;
import dto.user.UserRequest;
import dto.user.UserResponse;
import java.util.List;

/**
 * @author nguyen
 * @create_date 29/04/2022
 */
public interface UserRepo {
  void login(LoginRequest dto);

  void logout(LogoutRequest dto);

  UserResponse getById(int id);

  int addUser(UserRequest dto);

  int updateUser(int userId, UserRequest dto);

  void deleteUser(int id);

  List<UserResponse> search(String value);
}
