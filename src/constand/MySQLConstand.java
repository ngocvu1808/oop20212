package constand;

/**
 * @author nguyen
 * @create_date 29/04/2022
 */
public interface MySQLConstand {

  String URL = "jdbc:mysql://localhost:3306/open-shop";
  String USER_NAME = "root";
  String PASSWORD = "root";
  String CLASS_NAME = "com.mysql.jdbc.Driver";
}
