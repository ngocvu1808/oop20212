package constand.enumm;

/**
 * @author nguyen
 * @create_date 28/05/2022
 */
public enum Role {
  STAFF, ADMIN
}
