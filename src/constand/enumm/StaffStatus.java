package constand.enumm;

/**
 * @author nguyen
 * @create_date 27/05/2022
 */
public enum StaffStatus {
  ACTIVE, INACTIVE
}
