package dao.codegym;

import config.JDBCConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.codegym.User;

public class CustomerDao {

    public List<User> getAllCustomers() {
        List<User> userList = new ArrayList<>();

        Connection connection = JDBCConnection.getJDBCConnection();
        PreparedStatement pst = null;

        String sql = "SELECT * FROM user";

        try {
            pst = connection.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                User user = new User();

                user.setId(rs.getInt("ID"));
                user.setFullname(rs.getString("FULLNAME"));
                user.setAge(rs.getInt("AGE"));
                user.setUsername(rs.getString("USERNAME"));
                user.setPassword(rs.getString("PASSWORD"));
                user.setRetypepass(rs.getString("RETYPEPASS"));
                user.setPhone(rs.getString("PHONENUMBER"));
                user.setGender(rs.getString("GENDER"));
                user.setAddress(rs.getString("ADDRESS"));
                user.setRole(rs.getString("ROLE"));

                userList.add(user);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (pst != null) {
                try {
                    pst.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return userList;
    }

    public User getCustomerById(int id) {
        Connection connection = JDBCConnection.getJDBCConnection();
        PreparedStatement pst = null;

        String sql = "SELECT * FROM customer WHERE ID = ?";

        try {
            pst = connection.prepareStatement(sql);
            pst.setInt(1, id);
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                User user = new User();

                user.setId(rs.getInt("ID"));
                user.setFullname(rs.getString("FULLNAME"));
                user.setAge(rs.getInt("AGE"));
                user.setUsername(rs.getString("USERNAME"));
                user.setPassword(rs.getString("PASSWORD"));
                user.setRetypepass(rs.getString("RETYPEPASS"));
                user.setPhone(rs.getString("PHONENUMBER"));
                user.setGender(rs.getString("GENDER"));
                user.setAddress(rs.getString("ADDRESS"));
                user.setRole(rs.getString("ROLE"));

                return user;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (pst != null) {
                try {
                    pst.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return null;
    }
    
    public void addCustomer(User user) {
        Connection connection = JDBCConnection.getJDBCConnection();
        PreparedStatement pst = null;
        String sql = "INSERT INTO customer(FULLNAME, AGE, USERNAME, PASSWORD, RETYPEPASS, PHONENUMBER, GENDER, ADDRESS, ROLE) VALUE(?,?,?,?,?,?,?,?,?)";

        try {
            pst = connection.prepareStatement(sql);
            pst.setString(1, user.getFullname());
            pst.setInt(2, user.getAge());
            pst.setString(3, user.getUsername());
            pst.setString(4, user.getPassword());
            pst.setString(5, user.getRetypepass());
            pst.setString(6, user.getPhone());
            pst.setString(7, user.getGender());
            pst.setString(8, user.getAddress());
            pst.setString(9, user.getRole());

            int rs = pst.executeUpdate();
            System.out.println(rs);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (pst != null) {
                try {
                    pst.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public void updateCustomer(User user) {
        Connection connection = JDBCConnection.getJDBCConnection();
        PreparedStatement pst = null;
        String sql = "UPDATE customer SET FULLNAME = ?,AGE = ?,USERNAME = ?,PASSWORD = ?,RETYPEPASS = ?,PHONENUMBER = ?,GENDER = ?,ADDRESS = ?, ROLE = ? WHERE ID =?";

        try {
            pst = connection.prepareStatement(sql);
            pst = connection.prepareStatement(sql);
            pst.setString(1, user.getFullname());
            pst.setInt(2, user.getAge());
            pst.setString(3, user.getUsername());
            pst.setString(4, user.getPassword());
            pst.setString(5, user.getRetypepass());
            pst.setString(6, user.getPhone());
            pst.setString(7, user.getGender());
            pst.setString(8, user.getAddress());
            pst.setString(9, user.getRole());
            pst.setInt(10, user.getId());

            int rs = pst.executeUpdate();
            System.out.println(rs);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (pst != null) {
                try {
                    pst.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public void deleteCustomer(int id) {
        Connection connection = JDBCConnection.getJDBCConnection();
        PreparedStatement pst = null;
        String sql = "delete from Customer where id = ?";

        try {
            pst = connection.prepareStatement(sql);
            pst.setInt(1, id);
            int rs = pst.executeUpdate();
            System.out.println(rs);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (pst != null) {
                try {
                    pst.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
    
     public List<User> searchCustomers(String valToSearch) {
        List<User> userList = new ArrayList<>();

        Connection connection = JDBCConnection.getJDBCConnection();
        PreparedStatement pst = null;

        String sql = "SELECT * FROM `customer` WHERE CONCAT (`ID`, `FULLNAME`, `AGE`, `USERNAME`, `PHONENUMBER`) LIKE '%"+valToSearch+"%' ";

        try {
            pst = connection.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                User user = new User();

                user.setId(rs.getInt("ID"));
                user.setFullname(rs.getString("FULLNAME"));
                user.setAge(rs.getInt("AGE"));
                user.setUsername(rs.getString("USERNAME"));
                user.setPassword(rs.getString("PASSWORD"));
                user.setRetypepass(rs.getString("RETYPEPASS"));
                user.setPhone(rs.getString("PHONENUMBER"));
                user.setGender(rs.getString("GENDER"));
                user.setAddress(rs.getString("ADDRESS"));
                user.setRole(rs.getString("ROLE"));

                userList.add(user);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (pst != null) {
                try {
                    pst.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return userList;
    }
}
