package service.codegym;

import dao.codegym.CustomerDao;
import java.util.List;
import model.codegym.User;

public class CustomerService {

    private CustomerDao customerDao;

    public CustomerService() {
        customerDao = new CustomerDao();
    }

    public List<User> getAllCustomers() {
        return customerDao.getAllCustomers();
    }

    public void addCustomer(User user) {
        customerDao.addCustomer(user);
    }

    public void deleteCustomer(int id) {
        customerDao.deleteCustomer(id);
    }

    public User getCustomerById(int id) {
        return customerDao.getCustomerById(id);
    }

    public void updateCustomer(User user) {
        customerDao.updateCustomer(user);
    }

    public List<User> searchCustomers(String valToSearch) {
    return customerDao.searchCustomers(valToSearch);
    }
}
