package service.interf;

import dto.staff.StaffRequest;
import dto.staff.StaffResponse;
import service.impl.StaffServiceImpl;

/**
 * @author nguyen
 * @create_date 28/05/2022
 */
public interface StaffService {

  StaffService staffService = new StaffServiceImpl();

  int add(StaffRequest request);

  int update(int id, StaffRequest request);

  void delete(int id);

  StaffResponse get(int id);

}
