package service.impl;

import config.JDBCConnection;
import constand.Constand.StaffFields;
import constand.enumm.Gender;
import constand.enumm.Role;
import dto.staff.StaffRequest;
import dto.staff.StaffResponse;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import service.interf.StaffService;

/**
 * @author nguyen
 * @create_date 28/05/2022
 */
public class StaffServiceImpl implements StaffService {

  @Override
  public int add(StaffRequest request) {

    Connection connection = JDBCConnection.getJDBCConnection();
    PreparedStatement pst = null;
    String sql = "INSERT INTO " + StaffFields.TABLE
        + " (NAME, USERNAME, PASSWORD, CODE, PHONE, ADDRESS, SALARY, STATUS, GENDER, AGE, ROLE)"
        + " VALUE(?,?,?,?,?,?,?,?,?,?)";

    try {
      pst = connection.prepareStatement(sql);
      pst.setString(1, request.getName());
      pst.setString(2, request.getUsername());
      pst.setString(3, request.getPassword());
      pst.setString(4, request.getCode());
      pst.setString(5, request.getPhone());
      pst.setString(6, request.getAddress());
      pst.setString(7, request.getSalary());
      pst.setString(8, request.getStatus().name());
      pst.setString(9, request.getGender().name());
      pst.setInt(10, request.getAge());
      pst.setString(11, Role.STAFF.name());

      int rs = pst.executeUpdate();
      System.out.println(rs);

    } catch (Exception ex) {
      ex.printStackTrace();
    } finally {
      if (pst != null) {
        try {
          pst.close();
        } catch (SQLException ex) {
          ex.printStackTrace();
        }
      }
      if (connection != null) {
        try {
          connection.close();
        } catch (SQLException ex) {
          ex.printStackTrace();
        }
      }
    }
    return 0;
  }


  @Override
  public int update(int id, StaffRequest request) {
    Connection connection = JDBCConnection.getJDBCConnection();
    PreparedStatement pst = null;
    String sql = "UPDATE" + StaffFields.TABLE
        + " SET NAME = ?,USERNAME = ?,PASSWORD = ?,CODE = ?,PHONE = ?,ADDRESS = ?, SALARY = ?,STATUS = ?,GENDER = ? ,AGE = ? WHERE ID =?";

    try {
      pst = connection.prepareStatement(sql);
      pst.setString(1, request.getName());
      pst.setString(2, request.getUsername());
      pst.setString(3, request.getPassword());
      pst.setString(4, request.getCode());
      pst.setString(5, request.getPhone());
      pst.setString(6, request.getAddress());
      pst.setString(7, request.getSalary());
      pst.setString(8, request.getStatus().name());
      pst.setString(9, request.getGender().name());
      pst.setInt(10, request.getAge());
      pst.setInt(11, id);

      int rs = pst.executeUpdate();
      System.out.println(rs);
    } catch (Exception ex) {
      ex.printStackTrace();
    } finally {
      if (pst != null) {
        try {
          pst.close();
        } catch (SQLException ex) {
          ex.printStackTrace();
        }
      }
      if (connection != null) {
        try {
          connection.close();
        } catch (SQLException ex) {
          ex.printStackTrace();
        }
      }
    }
    return 0;
  }

  @Override
  public void delete(int id) {
    Connection connection = JDBCConnection.getJDBCConnection();
    PreparedStatement pst = null;
    String sql = "delete from " + StaffFields.TABLE + " where id = ?";

    try {
      pst = connection.prepareStatement(sql);
      pst.setInt(1, id);
      int rs = pst.executeUpdate();
      System.out.println(rs);
    } catch (Exception ex) {
      ex.printStackTrace();
    } finally {
      if (pst != null) {
        try {
          pst.close();
        } catch (SQLException ex) {
          ex.printStackTrace();
        }
      }
      if (connection != null) {
        try {
          connection.close();
        } catch (SQLException ex) {
          ex.printStackTrace();
        }
      }
    }
  }

  @Override
  public StaffResponse get(int id) {
    Connection connection = JDBCConnection.getJDBCConnection();
    PreparedStatement pst = null;
    String sql = "SELECT * from " + StaffFields.TABLE + " where id = ?";

    try {
      pst = connection.prepareStatement(sql);
      pst.setInt(1, id);
      ResultSet rs = pst.executeQuery();
      System.out.println(rs);
      while (rs.next()) {
        StaffResponse response = new StaffResponse();

        response.setName(rs.getString(StaffFields.NAME));
        response.setPhone(rs.getString(StaffFields.PHONE));
        response.setAge(rs.getInt(StaffFields.AGE));
        response.setAddress(rs.getString(StaffFields.ADDRESS));
        response.setGender(Gender.valueOf(rs.getString(StaffFields.GENDER)));

        return response;
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    } finally {
      if (pst != null) {
        try {
          pst.close();
        } catch (SQLException ex) {
          ex.printStackTrace();
        }
      }
      if (connection != null) {
        try {
          connection.close();
        } catch (SQLException ex) {
          ex.printStackTrace();
        }
      }
    }
    return new StaffResponse();
  }
}
